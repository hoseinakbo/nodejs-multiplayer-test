﻿using SocketIO;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NetworkManager : MonoBehaviour {

    public static NetworkManager instance;
    public Canvas joinGameCanvas;
    public Canvas gameCanvas;
    public SocketIOComponent socket;
    public InputField playerNameInput;
    public InputField ipInputField;

    public Player currentPlayer;
    public Player opponent;

    void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);
        DontDestroyOnLoad(gameObject);
    }
    /*
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.A))
        {
        }
    }*/

    public void JoinGame()
    {
        StartCoroutine(ConnectToServer());
    }

    void SetSocketFunctions()
    {
        socket.On("other player connected", OnOtherPlayerConnected);
        socket.On("play", OnPlay);
        socket.On("room full", OnRoomFull);
        socket.On("player set card", OnPlayerSetCard);
        socket.On("other player disconnected", OnOtherPlayerDisconnected);
    }

    #region Commands

    IEnumerator ConnectToServer()
    {
        socket.url = "ws://" + ipInputField.text.ToString() + ":3000/socket.io/?EIO=4&transport=websocket";
        socket.gameObject.SetActive(true);
        SetSocketFunctions();

        yield return new WaitForSeconds(1f);

        socket.Emit("player connect");

        yield return new WaitForSeconds(1f);

        string playerName = playerNameInput.text;
        PlayerJSON playerJSON = new PlayerJSON(playerName);
        string data = JsonUtility.ToJson(playerJSON);
        socket.Emit("play", new JSONObject(data));
        joinGameCanvas.gameObject.SetActive(false);
        gameCanvas.gameObject.SetActive(true);
    }

    public void CommandSetCard(string cardName, int cardStatNumber)
    {
        string data = JsonUtility.ToJson(new SetCardJSON(cardName, cardStatNumber));
        socket.Emit("player set card", new JSONObject(data));
    }

    public void CommandScoreChange()
    {

    }

    #endregion

    #region Listening

    void OnOtherPlayerConnected(SocketIOEvent socketIOEvent)
    {
        Debug.Log("Opponent joined");
        string data = socketIOEvent.data.ToString();
        UserJSON userJSON = UserJSON.CreateFromJSON(data);
        opponent.playerName = userJSON.name;
        opponent.score = userJSON.score;
        opponent.OnStatsChange();
    }
    void OnPlay(SocketIOEvent socketIOEvent)
    {
        Debug.Log("You joined");
        string data = socketIOEvent.data.ToString();
        UserJSON currentUserJSON = UserJSON.CreateFromJSON(data);
        currentPlayer.playerName = currentUserJSON.name;
        currentPlayer.score = currentUserJSON.score;
        currentPlayer.OnStatsChange();
    }
    void OnRoomFull(SocketIOEvent socketIOEvent)
    {
        Debug.Log("Room Full");

    }
    void OnPlayerSetCard(SocketIOEvent socketIOEvent)
    {
        Debug.Log("Card Set");
        string data = socketIOEvent.data.ToString();
        UserJSON userJSON = UserJSON.CreateFromJSON(data);
        if(userJSON.name == currentPlayer.playerName)
            currentPlayer.SetCard(userJSON.activeCardName, userJSON.activeCardStatNumber);
        else if(userJSON.name == opponent.playerName)
            opponent.SetCard(userJSON.activeCardName, userJSON.activeCardStatNumber);
        else
            Debug.Log("Incorrect name for card set");
    }
    void OnScore(SocketIOEvent socketIOEvent)
    {

    }
    void OnOtherPlayerDisconnected(SocketIOEvent socketIOEvent)
    {
        Debug.Log("Opponent disconnected");
        string data = socketIOEvent.data.ToString();
        UserJSON userJSON = UserJSON.CreateFromJSON(data);
        opponent.playerName = "";
        opponent.score = 0;
        opponent.OnStatsChange();
        opponent.SetCard("", 0);
    }

    #endregion

    #region JSONMessageClasses

    [Serializable]
    public class PlayerJSON
    {
        public string name;
        public PlayerJSON(string _name)
        {
            name = _name;
        }
    }

    [Serializable]
    public class SetCardJSON
    {
        public string activeCardName;
        public int activeCardStatNumber;

        public SetCardJSON(string _activeCardName, int _activeCardStatNumber)
        {
            activeCardName = _activeCardName;
            activeCardStatNumber = _activeCardStatNumber;
        }
    }

    [Serializable]
    public class UserJSON
    {
        public string name;
        public int score;
        public string activeCardName;
        public int activeCardStatNumber;

        public static UserJSON CreateFromJSON(string data)
        {
            return JsonUtility.FromJson<UserJSON>(data);
        }
    }

    #endregion
}
