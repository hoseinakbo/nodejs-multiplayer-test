﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Card : MonoBehaviour {

    public Text nameText;
    public Text statNumberText;

    public string cardName;
    public int cardStatNumber;
 
    public void CardOnClick()
    {
        NetworkManager.instance.currentPlayer.SetCard(cardName, cardStatNumber);
        NetworkManager.instance.CommandSetCard(cardName, cardStatNumber);
    }

    [ContextMenu("Refresh Card")]
    public void RefreshCard()
    {
        nameText.text = cardName;
        statNumberText.text = cardStatNumber.ToString();
    }
}
