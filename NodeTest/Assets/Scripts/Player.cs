﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Player : MonoBehaviour {

    public string playerName;
    public int score;
    string activeCardName;
    int activeCardStatNumber;

    public Text nameText;
    public Text scoreText;
    public Text cardNameText;
    public Text cardStatNumberText;

    public void OnStatsChange()
    {
        nameText.text = playerName;
        scoreText.text = score.ToString();
    }

    public void SetCard(string cardName, int cardStatNumber)
    {
        activeCardName = cardName;
        activeCardStatNumber = cardStatNumber;
        cardNameText.text = activeCardName;
        cardStatNumberText.text = activeCardStatNumber.ToString();
    }
}
