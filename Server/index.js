var app = require('express')();
var server = require('http').Server(app);
var io = require('socket.io')(server);

server.listen(3000);

var clients = [];

app.get('/', function(req, res) {
	res.send('yo dude');
});

io.on('connection', function(socket) {

	var currentPlayer = {};
	currentPlayer.name = 'unassigned';

	socket.on('player connect', function() {
		console.log(currentPlayer.name+' recv: player connect');
		for (var i = 0; i < clients.length; i++) {
			var playerConnected = {
				name:clients[i].name,
				score:clients[i].score
			}
			socket.emit('other player connected', playerConnected);
			console.log(currentPlayer.name+' emit: other player connected: '+JSON.stringify(playerConnected));
		}
	});

	socket.on('play', function(data) {
		console.log(currentPlayer.name+' recv: play: '+JSON.stringify(data));

		currentPlayer = {
			name:data.name,
			score:0
		}
		clients.push(currentPlayer);
		console.log(currentPlayer.name+' emit: play: '+JSON.stringify(currentPlayer));
		socket.emit('play', currentPlayer);
		socket.broadcast.emit('other player connected', currentPlayer);
	});		

	socket.on('player set card', function(data) {
		console.log(currentPlayer.name+'recv: player set card: '+JSON.stringify(data));
		currentPlayer.activeCardName = data.activeCardName;
		currentPlayer.activeCardStatNumber = data.activeCardStatNumber;
		socket.broadcast.emit('player set card', currentPlayer);
	});

	socket.on('disconnect', function() {
		console.log(currentPlayer.name+' recv: disconnect '+currentPlayer.name);
		socket.broadcast.emit('other player disconnected', currentPlayer);
		console.log(currentPlayer.name+' bcst: other player disconnected '+JSON.stringify(currentPlayer));
		for(var i=0; i<clients.length; i++) {
			if(clients[i].name === currentPlayer.name) {
				clients.splice(i,1);
			}
		}
	});

});

console.log('--- server is running ---');